/*
 * RandomNumbers.h
 *
 *  Created on: Aug 2, 2011
 *      Author: Rob
 */

#ifndef RANDOMNUMBERS_H_
#define RANDOMNUMBERS_H_

#include "MTRand.h"

namespace RandomNumbers {

	extern void bivariateGaussian(MTRand, double, double, double, double& , double& );
	extern double logNormal(MTRand, const double, const double);

};

#endif /* RANDOMNUMBERS_H_ */
