
#ifndef PRIMES_H_
#define PRIMES_H_


class Primes{
	
	private:
		bool isPrime(int x) {

			if (x < 2) { return false; }

			for (int i = 2; i<x; i++) {
				if (x % i == 0) { return false; }
			}
			return true;
		};
		
		int  currentPrime = 0;

	public : 	
		Primes();
		Primes(int startingNum) {
			currentPrime = startingNum;
		}

		double nextPrime() {
			currentPrime++;
			for (; !isPrime(currentPrime); ++currentPrime) {}

			return currentPrime;
		}
		double getNthPrime(int N) {
			int numPrimes = 0;
			int curPrime;

			for (int i = 2;; i++) {

				if (isPrime(i)) {
					numPrimes++;
					curPrime = i;
				}

				if (numPrimes >= N) { break; }
			}
			return curPrime;
		}

	
};

#endif
#include "Primes.h"





